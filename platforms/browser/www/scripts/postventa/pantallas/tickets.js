var tabla_visitas;	
var tipo_estado			= 0;
var arr_registros		= consulta_sql("SELECT * FROM DEMO",0);
console.log(arr_registros);
tabla_visitas = $('#tabla_tickets').DataTable({		
	"processing"		: true,
	"serverSide"		: true,
	"responsive"		: true,		
	"ajax"				: {
		"url"			: 'data_tables/postventa/visitas_coordinadas/dt_visitas_coordinadas.php',
		"type"			: 'POST',
		"data"			: function (d){
			return $.extend({}, d,{			
				"draw" 			: d.draw,
				"length" 		: d.length,
				"order" 		: d.order,
				"start" 		: d.start,
				"search" 		: d.search,
				"tipo_estado" 	: tipo_estado
			});
		}
	},
	"columns"			: [                    
		{ "data"		: "id_ticket",		"class": "text-center" },
		{ "data"		: "proyecto",		"class": "text-center" },
		{ "data"		: "unidad",			"class": "text-center" },
		{ "data"		: "torre",			"class": "text-center" },
		{ "data"		: "fecha",			"class": "text-center" },
		{ "data"		: "hora",			"class": "text-center" },
		{ "data"		: "estado",			"class": "text-center" },
		{ "data"		: "accion",			"class": "text-center", "orderable": false }
	],
	"order"				: [[ 5, "asc" ]],
	"rowId"				: 'extn',
	"select"			: true,	
	"stateSave"			: true,
	"lengthMenu"		: [[5, 10, 15, 20, 50, -1], [5, 10, 15, 20, 50, 'Todos']],
	"language"			: {
		"url"			: "language/data_tables/ES_CL.json",			
	},"initComplete"	: function(settings, json){		
		panel_header_data_table('#panel_inspecciones_coordinadas');						
	},"fnDrawCallback"	: function (oSettings){					
	}		
});
	
jQuery('#tabla_tickets tbody').on( 'click', 'tr', function () {
	if (jQuery(this).hasClass('selected')){
		jQuery(this).removeClass('selected');
	} else {
		tabla_visitas.$('tr.selected').removeClass('selected');
		jQuery(this).addClass('selected');
	}
});

function cambiar_filtro(id_estado){
	tipo_estado			= id_estado;
	tabla_visitas.draw();
}