function cargar_pagina(pagina, datos) {
    jQuery('#preloader').removeClass('hide-preloader').addClass('show-preloader').show();	
	jQuery("#contenido_pagina").html('');
    jQuery("#contenido_pagina").load(pagina, datos, function() {        
		jQuery('#preloader').removeClass('show-preloader').addClass('hide-preloader').delay(100).stop().fadeOut("slow");       //Esconder el Preloader
		$('.menu-hider, .close-menu, .menu-bar').trigger('click'); //Esconder el Menu
    });
}

function cargar_diccionario(){
	console.log("IMPLEMENTAR CARGA DE DICCIONARIO!");	
}

function ajustar_coverpage(){
	$(window).trigger('resize');	
}

function panel_header_data_table(id_panel){
	$(id_panel + ' .dataTables_filter input').attr('placeholder','Buscar...');	
	$(id_panel + ' .panel-ctrls').append($(id_panel + ' .dataTables_filter').addClass("pull-right")).find("label").addClass("panel-ctrls-center");
	$(id_panel + ' .panel-ctrls').append("<i class='separator'></i>");
	$(id_panel + ' .panel-ctrls').append($(id_panel + ' .dataTables_length').addClass("pull-left")).find("label").addClass("panel-ctrls-center");
	$(id_panel + ' .panel-footer').append($(id_panel + " .dataTable+.row"));
	$(id_panel + ' .dataTables_paginate>ul.pagination').addClass("pull-right m0");
}