var tabla_tickets;	
var arr_registros					= 	{ "data": [] };
//var arr_registros					= 	{ "data" : [ [1, "Proyecto 1", "201", "A", "2011-04-25", "10:00", "VISITA", ""],[2, "Proyecto 2", "202", "A", "2011-04-25", "13:00", "TRABAJO", ""]	] };
var tipo_estado						= 0;

function cargar_tickets(){
	db.readTransaction(function(t){
		var sql						= "SELECT * FROM TICKETS";	
		t.executeSql(sql, [], function(t, res){
			for (var i = 0; i < res.rows.length; i++){
				arr_registros.data.push(res.rows.item(i));
			}		
			cargar_tabla_tickets(arr_registros);			
		}, function (t, e){												
			console.log(e);
		});
	});
}
	
jQuery('#tabla_tickets tbody').on( 'click', 'tr', function () {
	if (jQuery(this).hasClass('selected')){
		jQuery(this).removeClass('selected');
	} else {
		tabla_tickets.$('tr.selected').removeClass('selected');
		jQuery(this).addClass('selected');
	}
});

function cambiar_filtro(id_estado){
	tipo_estado			= id_estado;
	tabla_tickets.draw();
}

function cargar_tabla_tickets(arr_datos){
	tabla_tickets			= $('#tabla_tickets').DataTable({		
		"processing"		: true,			
		"responsive"		: true,					
		"data"				: arr_datos,
		/*"columns"			: [                    
			{ "data"		: "id_ticket",		"class": "text-center" },
			{ "data"		: "proyecto",		"class": "text-center" },
			{ "data"		: "unidad",			"class": "text-center" },
			{ "data"		: "torre",			"class": "text-center" },
			{ "data"		: "fecha",			"class": "text-center" },
			{ "data"		: "hora",			"class": "text-center" },
			{ "data"		: "estado",			"class": "text-center" },
			{ "data"		: "accion",			"class": "text-center", "orderable": false }
		],
		*/
		"order"				: [[ 5, "asc" ]],
		"rowId"				: 'extn',
		//"select"			: true,	
		"stateSave"			: true,
		"lengthMenu"		: [[5, 10, 15, 20, 50, -1], [5, 10, 15, 20, 50, 'Todos']],
		"language"			: {
			"url"			: "scripts/postventa/datatables/language/ES_CL.json",			
		},"initComplete"	: function(settings, json){		
			panel_header_data_table('#panel_inspecciones_coordinadas');						
		},"fnDrawCallback"	: function (oSettings){					
		}		
	});
	
}

cargar_tickets();